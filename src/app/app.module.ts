import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';



import { AppComponent } from './app.component';
import { QuestionComponent } from './question/question.component';
import { ResultComponent } from './result/result.component';
import { QuizService } from './shared/quiz.service';
import { WelcomeComponent } from './welcome/welcome.component';

const appRoutes: Routes = [
  { path: '',redirectTo: '/welcome',pathMatch: 'full'},
  { path: 'welcome', component: WelcomeComponent },
  { path: 'question', component: QuestionComponent },
  { path: 'result', component: ResultComponent },
  { path: '**', component: WelcomeComponent }
  
];

@NgModule({
  declarations: [
    AppComponent,
    QuestionComponent,
    ResultComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
    ),
    HttpClientModule,
  ],
  
  providers: [QuizService],
  bootstrap: [AppComponent]
})
export class AppModule { }
