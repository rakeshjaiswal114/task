import { Component, OnInit } from '@angular/core';
import { QuizService } from './../shared/quiz.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css'],
})
export class ResultComponent implements OnInit {
  correct: any
  wrong: any
  constructor(private router: Router, private quizService: QuizService) {}

  ngOnInit() {
    this.correct = 0;
    this.wrong = 100;
    this.getReport()
  }

  getReport() {
    this.correct = (this.quizService.correctAnswerCount * 100) / 10
    this.wrong = 100 - this.correct
  }
}
