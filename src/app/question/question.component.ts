import { Component, OnInit } from '@angular/core';
import { QuizService } from './../shared/quiz.service';
import { Router } from "@angular/router";


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css'],
})
export class QuestionComponent implements OnInit {
  answers: any
  submittedAnswers: any
  errorMsg: boolean

  constructor(private router: Router, private quizService: QuizService) { }

  ngOnInit() {
    this.quizService.seconds = 0
    this.quizService.questionProgress = 0
    this.answers = []
    this.submittedAnswers = []
    this.errorMsg = false
    this.getQuestion()
    this.getAnswer()
  }

  getQuestion() {
    try {
      this.quizService.getQuestion()
        .subscribe(response => {
          this.quizService.question = response
          this.startTimer()
        }, error => {
          console.log('==Get error Details==', error)
        })
    } catch (ex) {
      console.log(ex)
    }
  }

  getAnswer() {
    try {
      this.quizService.getAnswer()
        .subscribe(response => {
          this.answers = response
        }, error => {
          console.log('==Get error Details==', error)
        })
    } catch (ex) {
      console.log(ex)
    }
  }

  startTimer() {
    this.quizService.timer = setInterval(() => {
      this.quizService.seconds++
      if (this.quizService.seconds == 120) {
        this.getResult()
        clearInterval(this.quizService.timer)
      }
    }, 1000)
  }

  answer(qId, choice) {
    this.quizService.question[this.quizService.questionProgress].answer = choice;
    this.submittedAnswers = this.quizService.question
    this.quizService.questionProgress++;
    if (this.quizService.questionProgress == 10) {
      this.onSubmit()
    }
  }

  onSubmit() {
    this.quizService.correctAnswerCount = 0;
    var len = this.submittedAnswers.length
    var flag = false
    this.errorMsg = true
    if (this.submittedAnswers.length == 0) {
      this.quizService.questionProgress = 0
    }
    else {
      for (var i = 0; i < len; i++) {
        if (this.submittedAnswers[i].answer == 0) {
          this.quizService.questionProgress = i
          flag = true
          break;
        }
      }
      if (!flag) {
        this.getResult()
      }
    }
  }

  getResult() {
    this.quizService.correctAnswerCount = 0;
    this.submittedAnswers.forEach((element, i) => {
      if (element.answer == this.answers[i]) {
        this.quizService.correctAnswerCount++
      }
    });
    this.router.navigate(['/result']);
    clearInterval(this.quizService.timer)
  }

  previous() {
    this.quizService.questionProgress--;
  }

  next() {
    this.quizService.questionProgress++;
  }

}

