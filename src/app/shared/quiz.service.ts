import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QuizService {
  question:any;
  seconds:number;
  timer;
  questionProgress:number;
  correctAnswerCount:number=0

  constructor(private http: HttpClient) { }

  displayTimer(){
    return Math.floor(this.seconds/60) + ':' + Math.floor(this.seconds%60)
  }

  getQuestion() {
    return this.http.get('assets/data.json');
  }
  getAnswer() {
    return this.http.get('assets/answer.json');
  }
}
